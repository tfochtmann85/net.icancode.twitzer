package com.randomcompany.twitzer.model;

import junit.framework.TestCase;

public class TwitzTest extends TestCase {

    Twitz twitz;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        twitz = new Twitz();
    }

    public void testGetFormattedText() throws Exception {
        String text = " lang=\"en\" data-aria-label-part=\"0\">#GeordieShore's #VickyPattison has done a Kim... #KimKardashian #Kardashians http://kardashians.trendolizer.com/2015/03/geordie-shores-vicky-pattison-has-done-a-kim.html … pic.twitter.com/z45SPfDh6b";
        String msg = "#GeordieShore's #VickyPattison has done a Kim... #KimKardashian #Kardashians http://kardashians.trendolizer.com/2015/03/geordie-shores-vicky-pattison-has-done-a-kim.html … pic.twitter.com/z45SPfDh6b";

        twitz.setText(text);

        assertEquals(twitz.getFormattedText(), msg);
    }
}