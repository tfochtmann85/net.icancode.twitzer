package com.randomcompany.twitzer.model;

import com.randomcompany.twitzer.enums.DateStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tobi on 19.03.2015.
 *
 * getting all information for maximum extensibility
 */
public class Twitz {

    private String url = null;
    private String text = null;
    private Date date = null;
    private String user = null;
    private String id = null;
    private String img = null;
    private String name = null;

    private static final String JSON_KEY_URL = "url";
    private static final String JSON_KEY_TEXT = "text";
    private static final String JSON_KEY_DATE = "date";
    private static final String JSON_KEY_USER = "user";
    private static final String JSON_KEY_ID = "id";
    private static final String JSON_KEY_IMG = "img";
    private static final String JSON_KEY_NAME = "name";

    Pattern twitzMessagePattern = Pattern.compile("^\\s?lang=\"\\w+\"\\s?data-aria-label-part=\"\\w+\">(.+){1,140}.*");

    public Twitz(){

    }

    public Twitz(String url, String text, Date date, String user, String id, String img, String name){
        this.url = url;
        this.text = text;
        this.date = date;
        this.user = user;
        this.id = id;
        this.img = img;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
    Parses the json object for needed information
     */
    public boolean fromJSONObject(JSONObject jsonObject){

        // init
        boolean successful = true;

        // set url
        try {
            setUrl(jsonObject.getString(JSON_KEY_URL));
        } catch (JSONException e) {
            successful = false;
            e.printStackTrace();
        }

        // set text
        try {
            setText(jsonObject.getString(JSON_KEY_TEXT));
        } catch (JSONException e) {
            successful = false;
            e.printStackTrace();
        }

        // set date
        try {

            // format date is not stable yet
            Object dateToken = jsonObject.get(JSON_KEY_DATE);

            // int -> timestamp
            if (dateToken instanceof Integer){
            
                // init date as time in milliseconds
                setDate(new Date(Long.parseLong(String.valueOf(dateToken)) * 1000));
                
            // timestamp as string is available too
            } else if (dateToken instanceof String && ((String) dateToken).matches("\\d+")){
            
                // init date as time in milliseconds
                setDate(new Date(Long.parseLong((String) dateToken) * 1000));
            }

        } catch (JSONException e) {
            successful = false;
            e.printStackTrace();
        }

        // set user
        try {
            setUser(jsonObject.getString(JSON_KEY_USER));
        } catch (JSONException e) {
            successful = false;
            e.printStackTrace();
        }

        // set id
        try {
            setId(jsonObject.getString(JSON_KEY_ID));
        } catch (JSONException e) {
            successful = false;
            e.printStackTrace();
        }

        // set img
        try {
            setImg(jsonObject.getString(JSON_KEY_IMG));
        } catch (JSONException e) {
            successful = false;
            e.printStackTrace();
        }

        // set name
        try {
            setName(jsonObject.getString(JSON_KEY_NAME));
        } catch (JSONException e) {
            successful = false;
            e.printStackTrace();
        }

        return successful;
    }

    public String getFormattedText(){

        // if a string is set
        if (getText() != null){

            // parse regex
            Matcher twitzTextMatcher = twitzMessagePattern.matcher(getText());

            // find matching group
            if (twitzTextMatcher.find()){

                // get twitz real message
                return twitzTextMatcher.group(1);
            }
        }

        return getText();
    }

    /*
    Req. 2.1, 2.2 matched
    */
    public DateStatus getDateStatus(){

        // if date is null
        if (getDate() == null){

            // unknown (Was not parseable)
            return DateStatus.UNKNOWN;

        } else {

            // get today
            Date today = new Date();

            // check if twitz's date is today
            if (getDate().getYear() == today.getYear() &&
                    getDate().getMonth() == today.getMonth() &&
                    getDate().getDay() == today.getDay()){
                return DateStatus.TODAY;
            // normal other date
            } else{
                return DateStatus.OTHER;
            }
        }
    }
}
