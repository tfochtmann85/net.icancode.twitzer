package com.randomcompany.twitzer.provider.task;

import android.os.AsyncTask;
import android.util.Log;

import com.randomcompany.twitzer.MainActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.URI;

/**
 * Created by tobi on 19.03.2015.
 */
public class GetTwitzListTask extends AsyncTask<URI, Void, JSONArray> {

    private MainActivity mainActivity;
    private static final int HTTP_STATUS_CODE_OK = 200;

    public GetTwitzListTask(MainActivity mainActivity){
        super();
        this.mainActivity = mainActivity;
    }

    @Override
    protected void onPreExecute(){

        /*
        Req. 3 matched
        */
        // start spinner
        mainActivity.startProgressBar();
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected JSONArray doInBackground(URI... params) {
        // NOTE: returning a JSONArray does not match the json.org specification

        // initialize
        JSONArray receivedArray = null;
        URI twitzUri;

        // read params
        if (params.length > 0){

            twitzUri = params[0];

            // get the default http client
            DefaultHttpClient httpClient = new DefaultHttpClient();

            // set a http get
            HttpGet httpGetRequest = new HttpGet(twitzUri);

            // init
            HttpResponse httpResponse = null;

            // parse the response
            try {
                httpResponse = httpClient.execute(httpGetRequest);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // if response successful
            if (httpResponse != null){

                // get the status code
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // check if the status code is valid
                if (statusCode == HTTP_STATUS_CODE_OK){

                    // parse the http entity
                    HttpEntity receivedEntity = httpResponse.getEntity();

                    // init content
                    String receivedData = null;

                    // get the containing data
                    try {
                        receivedData = EntityUtils.toString(receivedEntity);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // if data could be decoded
                    if (receivedData != null){

                        // now the magic happens
                        try {
                            receivedArray = new JSONArray(receivedData);

                            Log.w(getClass().getName(), "NOTE: returning a JSONArray does not match the json.org specification");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Log.w(getClass().getName(), "Server " + twitzUri.toString() + " returned no parseable object");
                    }

                } else {
                    Log.w(getClass().getName(), "Server " + twitzUri.toString() + " responded not like expected with status code " + statusCode);
                }

            } else {
                Log.w(getClass().getName(), "Could not get response from server " + twitzUri.toString());
            }

            Log.w(getClass().getName(), "returning a JSONArray does not match the json.org specification, it has to be a JSONObject.");
        } else {
            Log.w(getClass().getName(), "Empty uri parameter");
        }

        return receivedArray;
    }

    @Override
    protected void onPostExecute(JSONArray twitzList){
        /*
        Req. 3 matched
        */
        // terminate spinner
        mainActivity.terminateProgressBar();

        // call method for further processing
        mainActivity.twitzListReceived(twitzList);
    }
}
