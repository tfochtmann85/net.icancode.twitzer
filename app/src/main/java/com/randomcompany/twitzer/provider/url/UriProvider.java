package com.randomcompany.twitzer.provider.url;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by tobi on 19.03.2015.
 */
public class UriProvider {

    private static final String HTTP_SCHEME = "http";
    private static final String TWITZ_HOST = "176.58.126.236";
    private static final String TWITZ_PATH = "/twitzer";


    public static URI getTwitzFeedUri(){
        URI uri = null;

        try {
            uri = new URI(HTTP_SCHEME, TWITZ_HOST, TWITZ_PATH, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return uri;
    }
}
