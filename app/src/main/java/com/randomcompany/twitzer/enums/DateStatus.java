package com.randomcompany.twitzer.enums;

/**
 * Created by tobi on 19.03.2015.
 */
public enum DateStatus {
    TODAY,
    OTHER,
    UNKNOWN,
}
