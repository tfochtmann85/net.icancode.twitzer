package com.randomcompany.twitzer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.randomcompany.twitzer.R;
import com.randomcompany.twitzer.model.Twitz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by tobi on 19.03.2015.
 */
public class TwitzListAdapter extends ArrayAdapter<Twitz> {

    private Context mContext;
    private int resourceId;
    private ArrayList<Twitz> twitzList;

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param resource The resource ID for a layout file containing a TextView to use when
     *                 instantiating views.
     * @param twitzList  The list of twitz
     */
    public TwitzListAdapter(Context context, int resource, ArrayList<Twitz> twitzList) {
        super(context, resource, twitzList);

        this.mContext = context;
        this.resourceId = resource;
        this.twitzList = twitzList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // set the list item view
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // get the row view
        View rowView = inflater.inflate(resourceId, parent, false);

        // get the text view
        TextView textView = (TextView) rowView.findViewById(R.id.textView);

        // get the user name view
        TextView userNameView = (TextView) rowView.findViewById(R.id.userNameView);

        // get the date view
        TextView dateView = (TextView) rowView.findViewById(R.id.dateView);

        // get current twitz
        Twitz twitz = twitzList.get(position);

        // set values

        /*
        Req. 2 matched
        */
        
        // text message
        textView.setText(twitz.getFormattedText());

        // user name
        userNameView.setText(twitz.getUser());

        // message date
        switch (twitz.getDateStatus()){
            case TODAY:
                dateView.setText(mContext.getString(R.string.today));
                break;
            case UNKNOWN:
                dateView.setText(mContext.getString(R.string.date_unknown));
                break;
            default:
                dateView.setText((new SimpleDateFormat(mContext.getString(R.string.display_date_fmt))).format(twitz.getDate()));
        }

        return rowView;
    }
}
