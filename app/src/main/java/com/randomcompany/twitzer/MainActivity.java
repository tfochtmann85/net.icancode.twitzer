package com.randomcompany.twitzer;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.randomcompany.twitzer.adapter.TwitzListAdapter;
import com.randomcompany.twitzer.model.Twitz;
import com.randomcompany.twitzer.provider.task.GetTwitzListTask;
import com.randomcompany.twitzer.provider.url.UriProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.randomcompany.twitzer.R.string.progress_loading_twitz;

/*
From the specification:

Req. 1: The twitz should be loaded from their timeline API at http://176.58.126.236/twitzer/
Req. 2: The twitz should be presented in a list with each row showing twitz text, username and the date of the twitz according to our specification.
 - Unfortunately the Twitzer API is in early development, therefore appropriate safety measures are required when parsing the Twitzer timeline.
 Req. 2.1: If the date is today the app should display “today”.
 Req. 2.2: If the app is earlier than today, it should display the date as “Sep 28”
Req. 3: The application should display a spinner (progressbar) while the feed is being loaded.
Req. 4: The application should use the graphics provided by the Twitzer Corp. creative director.
Req. 5: The app is only required to support iOS 7 or Android 4
Req. 6: The app is only for mobile (no tablet)
*/

public class MainActivity extends ListActivity {

    private ProgressDialog progressDlg;

    private ArrayList<Twitz> twitzList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume(){
        super.onResume();

        triggerTwitzReload();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
        case R.id.action_privacy_policy:
            // TODO: show privacy policy; mandatory for each app
            return true;
        case R.id.action_reload:
            triggerTwitzReload();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void triggerTwitzReload(){

        // instantiate the task
        GetTwitzListTask gtlt = new GetTwitzListTask(this);

        // get the uri
        URI twitzUri = UriProvider.getTwitzFeedUri();

        // if the uri could not be created
        if (twitzUri == null){

            // let the user know...
            Toast.makeText(this, R.string.error_getting_twitz_uri, Toast.LENGTH_LONG).show();

        } else {

            // call execute, not get in order to not block the ui!!!
            gtlt.execute(twitzUri);

        }
    }
    
    /*
    Req. 1 matched
    */
    public void twitzListReceived(JSONArray twitzJSONArray){

        // check for valid value

        // communication error
        if (twitzJSONArray == null){

            // let the user know...
            Toast.makeText(this, R.string.error_communicating_with_server, Toast.LENGTH_LONG).show();

        } else
        // no twitz on server
        {
            if (twitzJSONArray.length() == 0) {

                // let the user know...
                Toast.makeText(this, R.string.info_no_twitz, Toast.LENGTH_LONG).show();

            } else {

                // clear the current list
                twitzList = new ArrayList<>();

                // iterate through JSONArray elements
                for (int i=0; i < twitzJSONArray.length(); i++) {

                    // init
                    JSONObject jsonObj = null;

                    // get current object
                    try {
                        jsonObj = twitzJSONArray.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // if successful parsed
                    if (jsonObj != null){

                        // generate new twitz object
                        Twitz twitz = new Twitz();

                        // fill with json info
                        boolean successful = twitz.fromJSONObject(jsonObj);

                        // if the parsing was successful
                        if (!successful){
                            Log.w(getClass().getName() + ".twitzListReceived()", "Could not get all information from json " + jsonObj.toString());
                        }

                        // append to arraylist (may not be complete)
                        twitzList.add(twitz);

                    } else {
                        Log.w(getClass().getName() + ".twitzListReceived()", "Could not parse json object at index " + i);
                    }
                }

                // sort twitz by date descending
                Collections.sort(twitzList, new Comparator<Twitz>() {
                    @Override
                    public int compare(Twitz twitz1, Twitz twitz2) {

                        // both null
                        if (twitz1.getDate() == twitz2.getDate()){
                            return 0;
                        } else
                        if (twitz1.getDate() == null){
                            return -1;
                        } else
                        if (twitz2.getDate() == null){
                            return 1;
                        } else {
                            return twitz2.getDate().compareTo(twitz1.getDate());
                        }
                    }
                });
            }
        }

        // reload the list view ( if an error occured, just show the last status)
        reloadListView();

    }

    /*
    Req. 2 matched
    */
    private void reloadListView(){

        // get the array list adapter
        TwitzListAdapter tla = new TwitzListAdapter(this, R.layout.twitz_list_item, twitzList);

        // apply the list adapter to the list
        setListAdapter(tla);


    }

    public void startProgressBar(){

        // set a new progressdialog
        progressDlg = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);

        // set the displaying message
        progressDlg.setMessage(getString(progress_loading_twitz));

        // show the dialog
        progressDlg.show();
    }

    public void terminateProgressBar(){

        // if progress dialog was initialized
        if (progressDlg != null){

            // if progress dialog is still shown
            if (progressDlg.isShowing()){

                // shift to background
                progressDlg.dismiss();
            }
        }

    }
}
